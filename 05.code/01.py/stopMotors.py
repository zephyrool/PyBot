from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor

def stop(motorHAT):
	motorHAT.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
	motorHAT.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
	motorHAT.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
	motorHAT.getMotor(4).run(Adafruit_MotorHAT.RELEASE)
