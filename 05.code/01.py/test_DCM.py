#!/usr/bin/python
from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor
import time
import atexit
import sys

		# create a default object, no changes to I2C address or frequency
mh = Adafruit_MotorHAT(addr=0x60)

		# recommended for auto-disabling motors on shutdown!
def turnOffMotors():
	mh.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
	mh.getMotor(4).run(Adafruit_MotorHAT.RELEASE)

atexit.register(turnOffMotors)


L_Motor = mh.getMotor(1)
R_Motor = mh.getMotor(4)

# speed goes from 0 to 255
	
print "left Forward! slow"
L_Motor.setSpeed(50)
L_Motor.run(Adafruit_MotorHAT.FORWARD)
time.sleep(0.5)

L_Motor.run(Adafruit_MotorHAT.RELEASE)
time.sleep(0.5)

print "left forward fast"
L_Motor.setSpeed(100)
L_Motor.run(Adafruit_MotorHAT.FORWARD)
time.sleep(0.5)

L_Motor.run(Adafruit_MotorHAT.RELEASE)
time.sleep(0.5)

print "left backward fast"
L_Motor.run(Adafruit_MotorHAT.BACKWARD)
time.sleep(0.5)

L_Motor.run(Adafruit_MotorHAT.RELEASE)
time.sleep(0.5)


print "R Forward! slow"
R_Motor.setSpeed(50)
R_Motor.run(Adafruit_MotorHAT.FORWARD)
time.sleep(0.5)

R_Motor.run(Adafruit_MotorHAT.RELEASE)
time.sleep(0.5)

print "R forward fast"
R_Motor.setSpeed(100)
R_Motor.run(Adafruit_MotorHAT.FORWARD)
time.sleep(0.5)

R_Motor.run(Adafruit_MotorHAT.RELEASE)
time.sleep(0.5)

print "R backward fast"
R_Motor.run(Adafruit_MotorHAT.BACKWARD)
time.sleep(0.5)

R_Motor.run(Adafruit_MotorHAT.RELEASE)
time.sleep(0.5)

print "exiting"
sys.exit(0)
