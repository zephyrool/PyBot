import time
import RPi.GPIO as GPIO
import os
import sys
import checkDistance
from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor
import stopMotors
import atexit
import sys
import random

timestep = 0.25
distance_limit = 20
forward_time =3
L_speed= 1500
R_speed= 1500
setup_pause = 3

GPIO.setmode(GPIO.BOARD)
trigPin = 8
echoPin = 10

GPIO.setup(trigPin, GPIO.OUT)
GPIO.setup(echoPin, GPIO.IN)
GPIO.output(trigPin, False)

motorHAT = Adafruit_MotorHAT(addr=0x60)
L_motor = motorHAT.getMotor(1)
R_motor = motorHAT.getMotor(4)
def stopMotors():
	motorHAT.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
	motorHAT.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
	motorHAT.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
	motorHAT.getMotor(4).run(Adafruit_MotorHAT.RELEASE)
atexit.register(stopMotors)

for x in range(0, setup_pause):
	time.sleep(0.5)
	print "setup . . ." + str(x) + " of " + str(setup_pause)

for x in range(0, 3):
	distance = checkDistance.distance(GPIO, trigPin, echoPin)
	print distance
	time.sleep(0.2)

def set_engines(L_vel, R_vel, duration, L_motor, R_motor, motorHAT):
	def run_engine(motor, vel):
		motor.run(Adafruit_MotorHAT.RELEASE)
		if vel > 0 :
			motor.run(Adafruit_MotorHAT.FORWARD)
		elif vel <0 :
			motor.run(Adafruit_MotorHAT.BACKWARD)
		else :
			motor.run(Adafruit_MotorHAT.RELEASE)

	L_motor.setSpeed(abs(L_vel))
	R_motor.setSpeed(abs(L_vel))
	run_engine(L_motor, L_vel)
	run_engine(R_motor,R_vel)
	time.sleep(duration)
	L_motor.run(Adafruit_MotorHAT.RELEASE)
	R_motor.run(Adafruit_MotorHAT.RELEASE)

def goForward(duration, timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT):
	steps = int(duration/timestep)
	for x in range(0,steps):
		distance = checkDistance.distance(GPIO,trigPin, echoPin)
		if distance > distance_limit:
			set_engines(L_speed, R_speed, timestep, L_motor, R_motor, motorHAT)	

def goTurn(direction, duration, timestep, L_speed, R_speed, L_motor, R_motor, motorHAT):
	if direction == 1:
	 	L_vel = L_speed
	 	R_vel = -R_speed
	else:
	 	L_vel = -L_speed
	 	R_vel = R_speed
	 
	steps = int(duration/timestep)
	for x in range(1,steps):
		set_engines(L_vel, R_vel, timestep, L_motor, R_motor, motorHAT)

def goTurnToClear(direction, timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT):
	if direction == 1:
	 	L_vel = L_speed
	 	R_vel = -R_speed
	else:
	 	L_vel = -L_speed
	 	R_vel = R_speed
	set_engines(L_vel, R_vel, timestep, L_motor, R_motor, motorHAT)
	distance = checkDistance.distance(GPIO, trigPin, echoPin)
	while distance < distance_limit:
		engines.set_engines(L_vel, R_vel, timestep, L_motor, R_motor, motorHAT)
		distance = checkDistance.distance(GPIO, trigPin, echoPin)

def goRandom(timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT):
	choice=random.randint(1, 3)
	if choice <3:
		print "fw " + str(forward_time)
		goForward(forward_time, timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT)
	elif choice == 3:
		direction=random.randint(1, 2)
		print "turn (1=L, 2=R) " + str(direction) 
		goTurnToClear(direction, timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT)

print "forward 0.5"
goForward(1, timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT)
print "turn left"
goTurnToClear(1, timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT)
print "forward 0.5"
goForward(1, timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT)
print "turn right"
goTurnToClear(2, timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT)
print "forward 0.5"
goForward(1, timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT)
#for x in range(0, 25): 
#	print x
#	goRandom(timestep, distance_limit, GPIO, trigPin, echoPin, L_speed, R_speed, L_motor, R_motor, motorHAT)

GPIO.cleanup()
sys.exit(0)
