import Adafruit_PCA9685
import time

# you can create a new object for each HAT with
L_pwm = Adafruit_PCA9685.PCA9685(address=0x40)

# Configure min and ma pulse lengths
pulse_low = 1000  # Min pulse length out of 4096
pulse_high = 3000  # Max pulse length out of 4096

L_pwm.set_pwm_freq(60)

# setPWM(self, channel, on, off)
# on / off between 0 and 4096 of the cycle

L_pwm.set_pwm(0, 0, pulse_low)
time.sleep(2)
L_pwm.set_pwm(0, 0, pulse_high)
time.sleep(2)
L_pwm.set_pwm(0, 0, 0)
print("done")
