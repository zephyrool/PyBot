from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor
import stopMotors
import time

def set_engines (L_vel, R_vel, duration, L_motor, R_motor, motorHAT):
	def run_engine(motor, vel):
		motor.run(Adafruit_MotorHAT.RELEASE)
		if vel > 0 :
			motor.run(Adafruit_MotorHAT.FORWARD)
		elif vel <0 :
			motor.run(Adafruit_MotorHAT.BACKWARD)
		else :
			motor.run(Adafruit_MotorHAT.RELEASE)

	L_motor.setSpeed(abs(L_vel))
	R_motor.setSpeed(abs(L_vel))
	run_engine(L_motor, L_vel)
	run_engine(R_motor,R_vel)
	time.sleep(duration)
