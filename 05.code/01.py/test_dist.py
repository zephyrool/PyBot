import time
import RPi.GPIO as GPIO
import os
import sys
import checkDistance
import engines
from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor
import atexit
import sys

timestep = 1
distance_limit = 10
setup_pause=4
L_speed= 3000
L_speed= 3000

GPIO.setmode(GPIO.BOARD)
trigPin = 8
echoPin = 10

GPIO.setup(trigPin, GPIO.OUT)
GPIO.setup(echoPin, GPIO.IN)
GPIO.output(trigPin, False)

motorHAT = Adafruit_MotorHAT(addr=0x60)
L_motor = motorHAT.getMotor(1)
R_motor = motorHAT.getMotor(4)
def stopMotors():
	motorHAT.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
	motorHAT.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
	motorHAT.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
	motorHAT.getMotor(4).run(Adafruit_MotorHAT.RELEASE)
atexit.register(stopMotors)

for x in range(0,setup_pause):
	time.sleep(1)
	print "setup . . ." + str(x) + " of " + str(setup_pause)

for x in range(0, 20):
	distance = checkDistance.distance(GPIO, trigPin, echoPin)
	print distance
	time.sleep(0.2)

GPIO.cleanup()
sys.exit(0)
