import time

def distance(GPIO,trigPin,echoPin):
	time.sleep(0.01)
	distanceList = [measuredistance(GPIO,trigPin, echoPin)]
	#sample size should be 2 or more
	sampleSize = 5
	for n in range(1,sampleSize):
		time.sleep(0.01)
		distanceList.append(measuredistance(GPIO,trigPin, echoPin))

	returnList  = [i for i in distanceList if i >2]
	distance = trimmed_mean(returnList)
	print "distance = " + str(distance)
	return distance

def measuredistance(GPIO,trigPin, echoPin):
	GPIO.output(trigPin, True)
	time.sleep(0.00001)
	GPIO.output(trigPin, False)

	StartTime = time.time()
	StopTime = time.time()

	# save StartTime
	while GPIO.input(echoPin) == 0:
			StartTime = time.time()

	# save time of arrival
	while GPIO.input(echoPin) == 1:
			StopTime = time.time()

	# time difference between start and arrival
	TimeElapsed = StopTime - StartTime
	# multiply with the sonic speed (34300 cm/s)
	# and divide by 2, because there and back
	measuredistance = (TimeElapsed * 34300) / 2
	return measuredistance
	
def trimmed_mean(lst):
    trimmed_lst = sorted(lst)[1:-1]
    return sum(trimmed_lst) / len(trimmed_lst)
